import './Button.scss';

interface ButtonProps {
    onClick?: () => void;
    title: string;
}

export const Button: React.FC<ButtonProps> = ({ onClick , title}) => {
    return (
        <button onClick={onClick} className='custom-button'>
            { title }
        </button>
    )
}
