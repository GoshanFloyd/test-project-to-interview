import {getPublicFolderPath} from "../../utils";

export const Logo = () => (
    <img src={`${getPublicFolderPath()}/img/logo.svg`}
        alt="Aviasales Logo"/>
);
