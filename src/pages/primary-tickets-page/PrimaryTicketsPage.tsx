import './PrimaryTicketsPage.scss'
import {Logo} from "../../components/logo";
import {TicketFilters} from "../../components/ticket-filters";
import {TicketSortTypePicker} from "../../components/ticket-sort-type-picker";
import {useContext, useEffect, useMemo, useState} from "react";
import {Ticket, TicketResponse, TicketSortType} from "../../types";
import {TickerCard} from "../../components/ticket-card";
import {Button} from "../../components/button";
import {SearchId} from "../../components/search-id-context";
import {filterTickets, sortTickets, usePooling} from "../../utils";
import axios from "axios";
import {TicketFilterType} from "../../types/ticket-filter-type";

export const PrimaryTicketsPage = () => {
    const { searchId } = useContext(SearchId);

    const ticketsRequest = useMemo(() => {
        if (searchId) {
            return axios.get('/api/tickets', {
                params: {
                    searchId,
                }
            })
        }
        return null;
    }, [searchId]);

    const [start, isLoading, ticketsData] = usePooling<TicketResponse>(ticketsRequest)

    const [selectedSortType, setSelectedSortType] = useState<TicketSortType>(TicketSortType.CHEAP);
    const [visibleAll, setVisibleAll] = useState<boolean>(false);
    const [tickets, setTickets] = useState<Ticket[]>([]);
    const [visibleTickets, setVisibleTickets] = useState<Ticket[]>([]);
    const [filters, setFilters] = useState<TicketFilterType[]>([]);

    useEffect(() => {
        if (searchId && !isLoading) {
            start();
        }
    }, [searchId, start, isLoading]);

    useEffect(() => {
        if (ticketsData) {
            setTickets(ticketsData.tickets);
        }
    }, [ticketsData]);

    useEffect(() => {
        setVisibleTickets(visibleAll ? tickets : tickets.slice(0, 5));
    }, [tickets, visibleAll]);

     const preparedTickets = useMemo(() => (
         filterTickets(filters, visibleTickets).sort(sortTickets(selectedSortType))
     ), [visibleTickets, selectedSortType, filters]);

    return (
        <>
            <div className='ticket-list__logo-container'>
                <Logo />
            </div>
            <div className="ticket-list__wrapper">
                <aside>
                    <TicketFilters onChange={setFilters} />
                </aside>
                <section className='ticket-list__right-side'>
                    <TicketSortTypePicker onChange={(data) => setSelectedSortType(data)} />
                    { preparedTickets.map(t => (
                        <TickerCard ticket={t} key={t.price+t.carrier} />
                    ))}
                    { !visibleAll && tickets.length > 5 && (
                        <Button
                            onClick={() => setVisibleAll(true)}
                            title={`Показать еще ${tickets.length - 5} билетов!`}
                        />
                    )}
                </section>
            </div>
        </>
    )
};
