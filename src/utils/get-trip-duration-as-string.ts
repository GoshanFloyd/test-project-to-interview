export const getTripDurationAsString = (duration: number): string => {
    return `${Math.floor(duration/24/60)}д ${Math.floor(duration/60%24)}ч ${Math.floor(duration%60)}м`;
}
