import {AxiosPromise} from "axios";
import {useCallback, useRef, useState} from "react";
import {PoolingResponse} from "../types";

export const usePooling = <T extends PoolingResponse>(
    request: AxiosPromise<T> | null,
    delay = 1000,
): [() => void, boolean, T | undefined] => {
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState<T>();
    const interval = useRef<number>();

    const poolRequest = useCallback(() => {
        interval.current = window.setInterval(() => {
            request!.then((res) => {
                setData(res.data);
                if (res.data.stop) {
                    setIsLoading(false);
                    clearInterval(interval.current);
                }
            }).catch(err => console.log(err));
        }, delay);
    }, [request, delay]);

    const startPooling = useCallback(() => {
        if (request) {
            clearInterval(interval.current);
            setIsLoading(true)
            poolRequest();
        }
    }, [setIsLoading, request, poolRequest]);

    return [startPooling, isLoading, data];
};
