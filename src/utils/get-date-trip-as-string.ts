export const getDateTripAsString = (dateString: string, duration: number) => {
    const fromDate = new Date(dateString);
    const toDate = new Date(dateString);
    toDate.setTime(fromDate.getTime() + (duration * 60 * 1000))

    const pad = (n: number) => n < 10 ? ('0' + n).slice(-2) : n;

    const fromLabel = `${pad(fromDate.getHours())}:${pad(fromDate.getMinutes())}`;
    const toLabel = `${pad(toDate.getHours())}:${pad(toDate.getMinutes())}`;

    return `${fromLabel} - ${toLabel}`;
}
