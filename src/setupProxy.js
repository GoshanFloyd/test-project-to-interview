const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(
        '/api',
        createProxyMiddleware({
            target: 'https://front-test.beta.aviasales.ru',
            pathRewrite: {
              '/api': ''
            },
            changeOrigin: true,
        })
    );
};
